#!/bin/sh

echo 'Building Docker Image'
docker build -t dependency-check/frontend .

echo 'Running Application'
docker run -it -p 8080:80 --rm --name dependency-check-app-1 dependency-check/frontend